package com.posapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PosappBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(PosappBackendApplication.class, args);
	}
}
