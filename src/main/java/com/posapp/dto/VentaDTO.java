package com.posapp.dto;

public class VentaDTO {

	private String nombre;
	private double importe;
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public double getImporte() {
		return importe;
	}
	public void setImporte(double importe) {
		this.importe = importe;
	}
	@Override
	public String toString() {
		return "VentaDTO [nombre=" + nombre + ", importe=" + importe + "]";
	}
}
