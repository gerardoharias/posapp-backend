package com.posapp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.posapp.model.Persona;

@Repository
public interface IPersonaDAO extends JpaRepository<Persona, Integer>{

}
