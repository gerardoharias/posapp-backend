package com.posapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.posapp.dao.IVentaDAO;
import com.posapp.model.Venta;
import com.posapp.service.IVentaService;

@Service
public class VentaServiceImpl implements IVentaService{
	
	@Autowired
	private IVentaDAO dao;

	@Override
	public Venta registrar(Venta venta) {
		venta.getDetalleVenta().forEach(vta -> vta.setVenta(venta));		
		return dao.save(venta);
	}

	@Override
	public Venta listarId(int idConsulta) {
		return dao.findOne(idConsulta);
	}

	@Override
	public List<Venta> listar() {
		return dao.findAll();
	}

	
	
}
