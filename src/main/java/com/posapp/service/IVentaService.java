package com.posapp.service;

import java.util.List;

import com.posapp.model.Venta;

public interface IVentaService {

	Venta registrar(Venta venta);
	
	Venta listarId(int idConsulta);
	
	List<Venta> listar();
}
